import MediaPlayer from './MediaPlayer.js'

const video = document.querySelector("video");
const button = document.querySelector("button");

const player = new MediaPlayer({el: video}); // se enbia en el objeto de configuracion el el
button.onclick = () => player.togglePlay();